extends "status_effect.gd"

# A multiplier for the damage dealt. Fine to use non-integer values
var poison_strength = 1

# Custom stat assignment is done in _init so that status effects can be
# compared before adding to the scene tree, since stacking cancels the
# addition of the object to both the tree and the turn order.
func _init():
	effect_name = "poison"
	effect_class = "poison"
	effect_type = TURN_BASED
	max_stack = 2
#	duration = 0
	

func start_turn():
	parent_entity.decrease_hp(int(2 * poison_strength))
	game_screen.describe("You feel the sting of poison")
	end_turn()
	

func set_poison_strength(strength):
	poison_strength = strength
	

func stack(new_effect):
	if stack == max_stack:
		return
	stack += 1
	poison_strength += 1