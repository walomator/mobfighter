extends Node

"""
Status effects are objects held by entities. Different status effects
do different things, but can be broken up into 3 major groups:
TURN_BASED, ON_HIT, and ON_ACTION. A status effect can also be in
multiple groups, usually ON_HIT or ON_ACTION also being TURN_BASED.

Breaking these status_effect groups into subclasses is not a good
idea, since groups are not mutually exclusive. Also, there would be
very few functions that could be added to the subclasses, and they
would likely be overloaded.

Here is what is expected of a custom implementation of a status_effect
in each group:
TURN_BASED - Add a start_turn function to do the effect, finish by
  calling end_turn
ON_HIT - Add an adjust_hit function to handle the damage dealt, finish
  by returning the modified amount.
ON_ACTION - This group has not been considered yet. An hypothetical
  effect is increase_damage.

Variables:
parent_entity
  When status effects are added to an entity, the entity adds the
  node as a child. Thus, the parent of this node will be the entity
  with the status effect at _ready.

game_screen

effect_name - Descriptive name as shown in-game

effect_class
  String to lump similar effects into a class like "poison"

secret_effect
  If you do not wish the player to be able to see the effect
  directly, this var will indicate that. It is up to the code that
  lists effects to respect this flag. Example effects could
  (possibly) include special effects from armor or semi-permanent
  buffs given by your class.

stack
  Stacks of an effect can cause custom behavior if implemented in the
  status_effect's script. See the "stack" function for how to count
  the stack in most custom status_effects.

max_stack - Stack limit before new additions are ignored.

_effect_groups
  An array that lists every group an effect is in. The list is made up
  zero or more of the enums TURN_BASED, ON_HIT, and ON_ACTION. This var
  is mostly useful for outside code, and is not used much internally.
  Use "set_effect_groups" as the setter.
  
  If adding another enum to represent a new kind of status effect, be
  sure to explicitly assign it the number twice the value of the
  previous enum, and always make sure the first enum is 1. This is so
  that Global.declump_enum may be used.

duration - The duration in turns. Set to 0 for no duration limit.
"""

signal turn_ended

var parent_entity
var game_screen
var effect_name
var effect_class
var secret_effect = false
var stack = 1
var max_stack = 1
enum {TURN_BASED = 1, ON_HIT = 2, ON_ACTION = 4}
var _effect_groups = []
var duration = 0

func _ready():
	parent_entity = get_parent()
	game_screen = Global.get_game_screen()
	

# Overload this function if _effect_groups is TURN_BASED
func start_turn():
	pass
	

# Don't overload this function just because the effect is TURN_BASED.
# This is all that is required of almost every conceivable effect.
func end_turn():
	if duration > 0:
		if duration == 1:
			die()
		duration -= 1
	# Else the effect has no duration limit
	
	emit_signal("turn_ended", self)
	

# Overload this function if _effect_groups is ON_HIT
func adjust_hit(amount):
	return amount
	

func die():
	# Remove the status effect from the turn order manually
	game_screen.turn_order.erase(self)
	game_screen.turn_position -= 1
	
	# Schedule object to be freed once it has informed game_screen its
	# turn is over
	self.connect("turn_ended", self, "queue_free")
	

func set_duration(new_duration):
	duration = new_duration
	

# The handler class for status effects (currently, entity.gd) is
# expected to handle the addition of a duplicate status effect. It
# should call this function and pass the StatusEffect object, then
# discard the StatusEffect object. By calling this func, the current
# status effect will merge with the new status effect as determined by
# the overloaded "stack" function of the particular status effect.
func stack(new_effect):
	pass
	# There is no good default behavior, but the following is almost
	# always good behavior to have:
	# ----------------------
	# if stack == max_stack:
	#	return
	# stack += 1
	# ----------------------
	

# To set the effect group, pass the desired group as an enum or, to set
# multiple groups, pass the sum of all desired groups. E.g.:
# set_effect_group(TURN_BASED + ON_ACTION)
# 
# This works because the enums are numbers, and the sum of any
# combination of these numbers are sure to be unique. Finally, the func
# "Global.declump_enum" is designed to break the sum back into its
# addends by making assumptions on how the numbers were assigned.

# See the documentation for Global.declump_enum to understand why any
# sum is sure to be unique and see documentation on _effect_groups
# above to see how to assign numbers to the enum of effects.
func set_effect_groups(group_enum):
	# The second arg is the highest value enum in the set. See
	# Global.declump_enum documentation as to why.
	_effect_groups = Global.declump_enum(group_enum, ON_ACTION)
	

func is_turn_based_effect():
	if TURN_BASED in _effect_groups:
		return true
	return false
	

func is_on_hit_effect():
	if ON_HIT in _effect_groups:
		return true
	return false
	

func is_on_action_effect():
	if ON_ACTION in _effect_groups:
		return true
	return false
	

func is_secret():
	return secret_effect
	