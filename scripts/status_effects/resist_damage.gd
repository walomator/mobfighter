extends "status_effect.gd"

# A multiplier for the damage received. 1 is damage immunity.
var damage_reduction = 1

func _init(damage_reduction = 1, default_effect_name = "resist damage"):
	effect_name = default_effect_name
	effect_type = ON_HIT
	

# FEAT - Implement limited uses
func adjust_hit(damage):
	damage = int(damage * damage_reduction)
	return damage
	
	
func set_damage_reduction(reduction):
	damage_reduction = reduction
	