extends Node

# DEV - This is needed by user_interface.gd in MobFighter, but using
#   custom parameters on a second callback created with
#   "add_connections" is a really confusing way to design simple_timer.

# DEV - Call this "advanced_timer.gd", and inherit a simple_timer from
#   the platformer_framework.

# simple_timer
# The simple_timer class is complex internally, but offers a simpler
# solution to tracking time than the built-in Timer class. Simply
# create an instance of this class and call its "start" function.
# Generally, it is used in the form start(self, timer_name, time).
# 
# "simple_timer" does not inherit Timer, but rather creates a _timer
# objects and works through it.
# 
# Users of this class will be passed back this object when the timer
# runs out of time, along with a timer name. This is passed back
# to the function "handle_timeout" in the initiator by default. You can
# change the callback function and the initiator in the arguments for
# the "start" function. In any case, you should queue_free() the timer
# object you are returned if you are done using the timer.

var _timer
var _timer_name # Useful for add_connection

func _init():
	_timer = Timer.new()
	

# start
# Args - initiator: the object to connect to the timeout signal
#      - new_timer_name: a name that is returned on callback
#      - time: the time that is set on the timer
#      - callback: You CAN change the callback function, but for most
#          cases "handle_callback" will be acceptable.
#      - additional_connections: You CAN add additional connections in
#          the start function instead of calling the function
#          "add_connection" separately. This is not normally
#          recommended, but will be necessary if you set a timer for 0
#          seconds.
# 
# Creates and sets up the Timer. It autostarts and is always a one-
# shot. It also always passes back a timer_name and the timer object
# itself.
# The timer object should be freed when it is passed back!
# 
func start(initiator, new_timer_name, time, callback = "handle_timeout", additional_connections = [[]]):
#	print("new timer. initiator: " + str(initiator) + ". name: " + str(timer_name) + ". time: " + str(time))
	_timer_name = new_timer_name
	initiator.add_child(self)
	self.add_child(_timer)
	
	# Recommended to use "add_connection" instead after running "start",
	# unless timer is set to 0 seconds, in which case this object would
	# have already emitted a signal.
	if additional_connections != [[]]:
		for additional_connection in additional_connections:
			if len(additional_connection) == 2:
				add_connection(additional_connection[0], additional_connection[1])
			if len(additional_connection) == 3:
				add_connection(additional_connection[0], additional_connection[1], additional_connection[2])
	
	_timer.connect("timeout", initiator, callback, [self, _timer_name])
	if time == 0:
		_timer.emit_signal("timeout")
	else:
		_timer.set_wait_time(time)
		_timer.set_one_shot(true)
		_timer.start()
	

func restart():
	_timer.start()
	

func set_wait_time(time):
	_timer.set_wait_time(time)
	

func get_time_left():
	return _timer.get_time_left()
	

func add_connection(initiator, callback, args = []):
	_timer.connect("timeout", initiator, callback, args)
	
