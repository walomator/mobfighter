extends Node

# DEV - This should be loaded from a file. What format to use?

#const monster_list = [
#	# Level 1
#	[
#		["small_slime"],
#		["goblin"],
#		["goblin_mage"],
#		["cobra"],
#		["goblin_bard"]
#	],
#	# Level 2
#	[
#		["medium_slime"],
#		["small_slime", "small_slime", "small_slime"],
#		["goblin", "goblin_mage"],
#		["goblin_bard"],
#		["goblin_bard", "goblin"],
#		["goblin_bard", "goblin_mage"],
#		["goblin_bard", "cobra"]
#	],
#	# Level 3
#	[
#		["slimy_serf", "medium_slime"],
#		["large_slime"],
#		["goblin", "goblin_mage", "goblin_bard"],
#		["odd_eye"],
#		["zombie"],
#		["odd_eye", "zombie"],
#		["zombie", "cobra"],
#		["wolf"]
#	]
#]

const monster_list = [
	# Level 1
	[
#		["goblin"],
#		["goblin_mage"],
#		["goblin", "goblin_mage"],
#		["goblin_mage", "goblin"],
#		["skeleton"]
		["tiny_wizard"],
		["fat_wizard"],
		["slime"]
	],
	# Level 2
	[
		["a_dvd_copy_of_shark_tale"]
	]
]

# Generates the array of mobs to spawn based on either the player's
# level at ui.player.level, or optionally at a level passed in as a
# parameter.
static func generate_names(encounter_level):
	# The level_list is a collection of possible encounters for each
	# player level. Each encounter is an array of mobs.
	var level_list = monster_list[encounter_level - 1]
	var selection = Global.randint(0, len(level_list) - 1)
	var mobs = level_list[selection]
	
	return mobs
	