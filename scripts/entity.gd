extends Node

# DEV - Document variables
# DEV - Make mobs and players have equivalent behavior for start_turn
#   and end_turn so that they both can inherit an entity.gd function
# DEV - Give strong types to attributes when supported in Godot 3.1.
# DEV - Provide getters for the effects arrays and make the arrays
#   themselves private.
# Inherit from this class instead of using it directly. Copy the _init
# function that is commented below into every script that inherits
# entity.gd, as _init functions cannot themselves be inherited.
# 

signal turn_ended
signal entity_defeated

var ui
var entity_name = "missing_name"
var level
var xp
var max_hp
var hp
var max_mp
var mp
var strn
var con
var dex
var foc
var wis
var intl
var all_effects = []
var turn_based_effects = []
var on_hit_effects = []
var on_action_effects = []
var spellbook

const Spellbook = preload("res://scripts/spellbook.gd")

# This function should be implemented in any class that inherits
# entity.gd. It is commented out because there is no good default
# behavior.
# 
#func start_turn():
#	pass
#	

# This function should be implemented in any class that inherits
# entity.gd. It is commented out because there is no good default
# behavior.
# 
#func end_turn():
#	emit_signal("turn_ended", self)
#

func _ready():
	ui = Global.get_game_screen()
	spellbook = Spellbook.new()
	self.add_child(spellbook)
	

# Setters for variables
# These setters perform some functions automatically when points cross
# certain thresholds, like HP passing below 0. However, the setters do
# not do rigid parameter control. For example, increase_hp(-10) may
# cause unwanted behavior.
# 
# Also, these functions are designed to be overloaded if necessary, so
# more complex functions like increase_hp should use the more primitive
# functions like set_hp.
func set_name(new_name):
	entity_name = new_name
	name = new_name # This is nice for debugging purposes, but otherwise don't use "name"
	
func set_level(new_level):
	level = round(new_level)
	
func set_xp(amount):
	xp = round(amount)
	
func set_max_hp(amount):
	max_hp = round(amount)
	if hp:
		if hp > max_hp:
			hp = max_hp
	
func set_max_mp(amount):
	max_mp = round(amount)
	if mp:
		if mp > max_mp:
			mp = max_mp
	
func set_hp(amount):
	hp = round(amount)
	if hp <= 0:
		emit_signal("entity_defeated", self)
	elif hp > max_hp:
		hp = max_hp
	
func set_mp(amount):
	mp = round(amount)
	if mp <= 0:
		mp = 0
	elif mp > max_mp:
		mp = max_mp
	
func set_strn(amount):
	strn = round(amount)
	
func set_con(amount):
	con = round(amount)
	
func set_dex(amount):
	dex = round(amount)
	
func set_foc(amount):
	foc = round(amount)
	
func set_wis(amount):
	wis = round(amount)
	
func set_intl(amount):
	intl = round(amount)
	
func increase_xp(amount):
	set_xp(xp + round(amount))
	
func increase_max_hp(amount):
	set_max_hp(max_hp + round(amount))
	
func increase_max_mp(amount):
	set_max_mp(max_mp + round(amount))
	
func increase_hp(amount):
	set_hp(hp + round(amount))
	
func increase_mp(amount):
	set_mp(mp + round(amount))
	
func increase_strn(amount):
	set_strn(strn + round(amount))
	
func increase_con(amount):
	set_con(con + round(amount))
	
func increase_dex(amount):
	set_dex(dex + round(amount))
	
func increase_foc(amount):
	set_foc(foc + round(amount))
	
func increase_wis(amount):
	set_wis(wis + round(amount))
	
func increase_intl(amount):
	set_intl(intl + round(amount))
	
func decrease_xp(amount):
	set_xp(xp - round(amount))
	
func decrease_max_hp(amount):
	set_max_hp(max_hp - round(amount))
	
func decrease_max_mp(amount):
	set_max_mp(max_mp - round(amount))
	
func decrease_hp(amount):
	set_hp(hp - round(amount))
	
func decrease_mp(amount):
	set_mp(mp - round(amount))
	
func decrease_strn(amount):
	set_strn(strn - round(amount))
	
func decrease_con(amount):
	set_con(con - round(amount))
	
func decrease_dex(amount):
	set_dex(dex - round(amount))
	
func decrease_foc(amount):
	set_foc(foc - round(amount))
	
func decrease_wis(amount):
	set_wis(wis - round(amount))
	
func decrease_intl(amount):
	set_intl(intl - round(amount))
	
# Adjusts hp decretion by on_hit_effects. To use this, overload
# "decrease_hp" in an implementation and have it call this function. To
# keep a means of decretion without adjustment, you can also implement
# a _decrease_hp func and use that.
func _adjust_and_decrease_hp(amount):
	for effect in on_hit_effects:
		amount = effect.adjust_hit(amount)
	set_hp(get_hp() - amount)

# Getters for variables
# These are not used internally, but are recommended to be used
# externally, primarily to match the paradigm of doing all stat
# manipulation with function calls.
func get_name():
	return entity_name
	
func get_level():
	return level
	
func get_xp():
	return xp
	
func get_max_hp():
	return max_hp
	
func get_max_mp():
	return max_mp
	
func get_hp():
	return hp
	
func get_mp():
	return mp
	
func get_strn():
	return strn
	
func get_con():
	return con
	
func get_dex():
	return dex
	
func get_foc():
	return foc
	
func get_wis():
	return wis
	
func get_intl():
	return intl
	

# Other stat-manipulating functions
func level_up():
	level += 1
	
func max_out():
	set_hp(max_hp)
	set_mp(max_mp)
	
func max_out_hp():
	set_hp(max_hp)
	
func max_out_mp():
	set_mp(max_mp)
	

# Other entity-mutating functions

# Takes an effect NAME, not OBJECT.
func add_effect(effect_name):
	# Load the script file from the given name
	effect_name.replace(" ", "_")
	var StatusEffect = load("res://scripts/status_effects/" + effect_name + ".gd")
	if not StatusEffect:
		ui.update_log("Problem loading StatusEffect \"" + effect_name + "\"!", ui.ERROR)
	var new_effect = StatusEffect.new()
	
	# Incorporate "status stacking" if the player already has the effect.
	var existing_effect
	for effect in all_effects:
		if effect.effect_class == new_effect.effect_class:
			existing_effect = effect
	if existing_effect:
			# All status effects can _theoretically_ be stacked
			existing_effect.stack(new_effect)
	else:
		self.add_child(new_effect)
	
		# Add the status effect to a list for its type and the master list
		if new_effect.effect_type == new_effect.TURN_BASED:
			turn_based_effects.append(new_effect)
			new_effect.connect("turn_ended", ui, "handle_turn_ended")
			ui.add_to_turn_order(new_effect)
		
		elif new_effect.effect_type == new_effect.ON_HIT:
			on_hit_effects.append(new_effect)
		
		elif new_effect.effect_type == new_effect.ON_ACTION:
			on_action_effects.append(new_effect)
		
		all_effects.append(new_effect)
	

# Other functions

# Overload this to have custom spell-casting conditions. Call this func
# to try expending MP of amount "cost" to cast a spell. Returns true if
# the MP was successfully spent.
func _initiate_spell(spell_name):
	var success = false
	if spellbook.knows_spell(spell_name):
		var spell = spellbook.get_spell(spell_name)
		if spell:
			if get_mp() >= spell.COST:
				success = true
				decrease_mp(spell.COST)
	return success
	

func cast_spell(spell_name, spell_args):
	var success = _initiate_spell(spell_name)
	if success:
		var spell = spellbook.get_spell(spell_name)
		spell.cast(self, spell_args)
		
	end_turn()
	

func die():
	self.queue_free()
	