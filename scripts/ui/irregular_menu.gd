extends "res://scripts/ui/list_menu.gd"

# DEV - Document this class

const IrregularCursor = preload("res://scripts/ui/irregular_cursor.gd")

func _forwarded_input(event):
	if cursor.locked == false:
		if event.is_action_pressed("ui_up"):
			cursor.decrement_selection()
		elif event.is_action_pressed("ui_down"):
			cursor.increment_selection()
		elif event.is_action_pressed("ui_right") or event.is_action_pressed("ui_accept"):
			cursor.select()
		
		elif leavable and event.is_action_pressed("ui_left"):
			leave_menu()
	
