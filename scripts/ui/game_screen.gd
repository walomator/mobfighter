extends "res://scripts/ui/user_interface.gd"

# BUG - Sometimes mobs are removed from the mobs array but not the turn
#   order or from the visible screen. This seems to occur when the mob
#   is defeated. This is no known way to trigger this bug except try
#   attacking a mob and then using a javelin on it.
#   Update: At least two possible crashes can happen. One is:
#   Invalid get index 'name' (on base: 'previously freed instance').
#   The other known crash happens when calling
#   gladiator_skills_menu.handle_mob_selected. See details in the func
#   description.
# 
# DEV - Currently function "describe" and the queue system are closely
#   tied. This will make it hard to adapt to improvements in describe
#   such as letter-by-letter typing. Consider yield?
# 
# DEV - Make turn_position and turn_order private and set up getters
#   and setters
# 
# DEV - Describe in its own paragraph how the game screen utilizes the
#   queue system.
# 
# DEV - Describe how the turn order is handled. It is a little
#   confusing, for example see that start_next_turn is already queued
#   when the turn order could have changed, and it must catch special
#   cases.
# 
# ---------------------------------------------------------------------
# 
# The game screen directly manages its specific UI elements, like
# MobArea and BattleDescription. Most of these nodes do not need
# scripts of their own, in which case they can be sufficiently mutable
# through the game_screen class.
#
# The game screen also manages starting an encounter and maintaining
# the turn order.
# 
# Variables:
# mob_area
#   This is an HBoxContainer where mob images can be added by
#   create_mob_container. It is expected to be preexisting and have as
#   much room as possible to draw mobs in.
# 
# battle_description
#   A Label of the current text in the bottom left, it is used by the
#  "describe" function. It is expected to be preexisting.
# 
# title
#   A Label that goes in the top left and is usually a mob name. It is
#   expected to be preexisting.
# 
# health bar
#   Instance of health_bar.gd, can be manipulated with
#   "set_health_bar". Note that the player class is designed to always
#   take care of updating the health_bar itself. Expected to be
#   preexisting.
# 
# action_menu
#   Instance of "action_menu.gd". The primary menu for doing things,
#   hence the name. Expected to be preexisting.
# 
# mob_select_menu
#   Instance of "mob_select_menu.gd". Should be the same size and
#   location as mob_area. Expected to be preexisting.
# 
# EncounterGenerator
#   Generator script used every start_encounter. Used statically.
# 
# QueueHandler
#   A script to separate the logic of timing the next call to some
#   function in this class.
# 
# queue_handler
#   Instance of QueueHandler. Not preexisting on startup.
# 
# player
#   created at _ready() as an entity
# 
# mobs
#   A list of mob objects that are alive in the encounter. It is not
#   currently useful to the game logic, but is kept up to date and can
#   help find some bugs. If the mobs array is not in the expected state
#   during some manipulation, the game can correct the mobs array and
#   Global.update_log with a WARNING.
# 
# turn_order
#   A list of entity objects that will take a turn. Therefore each
#   should have "start_turn" function. "start_next_turn" will cycle
#   through the turn order until the encounter is ended with
#   "end_encounter".
# 
# turn_postition
#   This will index which entity object in turn_order is taking its
#   turn. It is incremented on "handle_turn_ended". It is 0 indexed.
# 
# total_turns
#   A count of the total turns started for the current encounter
# 
# total_rounds
#   A count of the total rounds (i.e. cycles of the turn_order) started
#   for the current encounter. Includes the first round.
# 
# total_encounters
#   A count of the total encounters started
# 
# mob_container_scene
#   See "create_mob_container" function

# signals
signal mob_selected

# UI element nodes
var mob_area
var battle_description
var title
var health_bar
var action_menu
var mob_select_menu

# External scripts
const EncounterGenerator = preload("res://scripts/encounter_generator.gd")
const QueueHandler = preload("res://scripts/queue_handler.gd")
var queue_handler

# Combat related variables
var player
var mobs = []
var turn_order = []
var turn_position = 0
var total_turns = 0
var total_rounds = 0
var total_encounters = 0

# Packed scenes used by the UI
var mob_container_scene = preload("res://scenes/ui/MobContainer.tscn")
var pause_menu_scene = preload("res://scenes/ui/PauseMenu.tscn")

func _ready():
	# Set up the queue_handler. This class has functions to control
	# queue_handler indirectly, such as queue_event, queue_wait, etc.
	queue_handler = QueueHandler.new()
	self.add_child(queue_handler)
	
	# Track the various UI elements
	mob_area = get_node("MobArea")
	title = get_node("Title")
	battle_description = get_node("BattleDescription")
	health_bar = get_node("HealthBar")
	action_menu = get_node("ActionMenu")
	mob_select_menu = get_node("MobSelectMenu")	
	mob_select_menu.connect("mob_selected", self, "handle_mob_selected")
	mob_select_menu.connect("menu_left", self, "handle_menu_left")
	
	# Create the player
	player = create_entity("player")
	health_bar.set_max_fill_points(player.get_max_hp())
	health_bar.set_fill_points(player.get_hp())
	
	# Connect Global triggers to handlers in this UI
	Global.connect("reset_pressed", self, "handle_reset_pressed")
	Global.connect("debug_pressed", self, "handle_debug_pressed")
	Global.connect("pause_pressed", self, "handle_pause_pressed")
	
	start_encounter()
	

# Starts an encounter with new mobs. It does not clean up an old
# encounter; that is the job of end_encounter.
# start_encounter does four things:
# 1. Generates the new mobs using EncounterGenerator
# 2. Sets the turn order
# 3. Updates the display to show the first mob's name
# 4. Starts the turn of the first entity with start_next_turn
# 
# FEAT - Allow mobs to go first in some situations
func start_encounter():
	total_encounters += 1
	Global.update_log("\n\n"
			        +"---------------------" + "\n"
			        +"starting encounter " + str(total_encounters) + "\n"
			        +"---------------------", DEBUG)
	
	# Generate the new mobs
	var new_mob_names = EncounterGenerator.generate_names(player.level)
	for new_mob_name in new_mob_names:
		create_mob(new_mob_name)
	
	# Set the turn order for this encounter.
	# Add all turn_based_effects of entities to the position just before.
	for player_effect in player.turn_based_effects:
		turn_order.append(player_effect)
	turn_order.append(player)
	for mob in mobs:
		for mob_effect in mob.turn_based_effects:
			turn_order.append(mob_effect)
		turn_order.append(mob)
	Global.update_log("    turn order: " + str(turn_order), DEBUG)
	
	# Update display to show new mobs
	# FEAT - Support conditional showing of enemy names
	set_title(mobs[0].entity_name)
	if len(mobs) > 1:
		describe("A group of mobs approach!")
	else:
		# FEAT - Support correct articles
		describe("A " + mobs[0].entity_name.to_lower() + " approaches!")
	
	# Start the first entity's turn
	turn_position = 0
	queue_event("start_next_turn", null, 0)
	
# Ends the encounter. Should be triggered by defeating all mobs. If
# there is a queued event to end the turn of the current entity, the
# next turn won't be started, as start_next_turn will catch the special
# case of an empty turn_order.
func end_encounter():
	# Clear data from the previous encounter
	turn_order = []
	if mobs != []:
		Global.update_log("not all mobs were cleared from the previous encounter!", WARNING)
		mobs = []
	clear_mob_area()
	
	# Show the victory screen. Additional inter-encounter events could be
	# queued here.
	queue_event("show_victory")
	
	Global.update_log("trying to start encounter", DEBUG)
	queue_event("start_encounter", null, 0)
	

# FEAT - Not a very rich victory
func show_victory():
	describe("You won!")
	

# Uses the global scene switcher. There is currently no coming back
# from a game over except to restart.
# FEAT - Flesh out game over screen
func show_game_over():
	queue_handler.destroy_queue()
	Global.show_game_over()
	

# This function has not been rigorously tested for bugs
func add_to_turn_order(object, position = turn_order.size()):
	if position <= turn_position:
		# Then the index of the next object in the turn order has shifted
		turn_position -= 1
	
	turn_order.insert(position, object)
	

func erase_from_turn_order(object):
	var object_position = turn_order.find(object)
	if object_position != -1:
		if object_position <= turn_position:
			# Then the index of the next object in the turn order has shifted
			turn_position -= 1
	

# In addition to spawning an entity as an object, this script connects
# turn_ended and entity_defeated signals to the UI for dealing with the
# turn order. "Mob" entities themselves will call
# ui.create_mob_container in their _ready function. 
# 
# All "create_" functions should return the thing they created.
func create_entity(entity_name, script_path = "res://scripts/"):
	var EntityScript = load(script_path + entity_name.to_lower() + ".gd")
	var entity = EntityScript.new()
	
	# By adding to the scene, _ready is called, where the mob will add
	# itself as a mob_container to game_screen.
	self.add_child(entity)
	
	entity.connect("turn_ended", self, "handle_turn_ended")
	entity.connect("entity_defeated", self, "handle_entity_defeated")
	
	return entity
	

# An extension of create_entity to do all the extra work for adding
# mobs, but not necessarily adding them to the turn order.
func create_mob(mob_name):
	var mob = create_entity(mob_name, "res://scripts/mobs/")
	if mob == null:
		Global.update_log("Mob not found in \"create_mob\"!", ERROR)
	mobs.append(mob)
	return mob
	

# Removes tracking on the mob, then calls mob_node.die(), which has
# been designed to also handle mob_containers. This function assumes
# mob_node was in the turn_order, even though create_mob does not add
# it to the turn order.
func remove_mob(mob_node):
	mobs.erase(mob_node)
	erase_from_turn_order(mob_node)
#	turn_order.erase(mob_node)
	
	# Orphan the container and queue_free the mob.
	mob_node.die()
	

# mob_containers are added to the MobArea node. Called by mob_entities
# in _ready. This function loads the texture based on the filename,
# and mob_container nodes will align the texture onto the MobArea
# node when given the loaded texture with "mob_container.add_texture".
func create_mob_container(texture_filename):
	var mob_container = mob_container_scene.instance()
	mob_area.add_child(mob_container)
	
	var mob_texture = load("res://textures/mobs/" + texture_filename)
	mob_container.add_texture(mob_texture)
	
	return mob_container
	

func clear_mob_area():
	for container in mob_area.get_children():
		container.clear_area()
	

func set_title(mob_name):
	title.text = mob_name
	

func describe(description_message):
#	if description_locked == true:
#		return
	battle_description.text = description_message
	

func set_health_bar(count):
	health_bar.set_fill_points(count)
	

# Should be called by the action menu. The action menu connects the
# mob_selected signal emitted here to itself. The mob_select_menu uses
# an irregular_cursor object, which requires coordinates for every
# item, so the monsters in the mob_area are checked for their position.
func open_mob_select_menu():
	# DEV - This sets the irregular_positions in order of mob_container's
	#   texture_rects received. Could this come in out of visual order in
	#   any edge case?
	var irregular_positions = []
	for mob_container in mob_area.get_children():
		var mob_texture = mob_container.texture_rect
		if mob_texture != null:
			irregular_positions.append(mob_texture.rect_global_position)
	
	mob_select_menu.set_irregular_positions(irregular_positions)
	switch_active_menu(mob_select_menu)
	

func open_pause_menu():
	var pause_overlay = create_sub_ui()
	sub_ui.create_menu(sub_ui.ACTIVE, pause_menu_scene)
	

# See documentation in queue_handler.gd
func queue_event(callback, message = null, time = Global.game_speed):
	queue_handler.queue_event(callback, message, time)
	

# Queues a "wait" event, where the function called is the empty "_wait"
# function. This can be used to insert a pause time before a subsequent
# event rather than/in addition to after the subsequent event.
func queue_wait(wait_time = Global.game_speed):
	queue_event("_wait", null, wait_time)
	

# This _wait function is used by queue_wait to insert some time where
# nothing happens. The event to call _wait is queued. Once the event
# fires, queue_handler will pause for the time set. So _wait itself
# should do nothing.
func _wait():
	pass
	

# DEV - By queuing the function "_wait", it is not clear while
#   looking at dev logs that a description is queued.
func queue_describe(description, time = Global.game_speed):
	queue_event("_wait", description, time)
	


# Forwards the signal by reemitting it. The object to receive the
# signal is set by the object that called open_mob_select_menu() in the
# first place.
func handle_mob_selected(selection):
	emit_signal("mob_selected", selection)
	

func handle_menu_left(menu):
	switch_active_menu(action_menu)
	

# Adds a wait ahead of starting the next turn. This is not formally an
# "event" because it is not triggered by the event_handler, but by a
# signal sent out by an entity, hence putting the wait in front.
# 
# DEV - Passed parameter "entity" is not used. Explain why.
# DEV - Instead of inserting a pause at the end of the turn, each
#   entity should use a queue_describe
# DEV - Description of how the game screen uses the queue system should
#   be in the class description, and a reference should be made to it
#   here.
func handle_turn_ended(entity):
	Global.update_log("ending turn " + str(turn_position), DEBUG)
	turn_position += 1
	total_turns += 1
	
	queue_wait()
	queue_event("start_next_turn", null, 0)
	

# Determines "next_entity", according to the turn_order, and starts
# this entity's turn.
# 
# It is queued by handle_turn_ended to increment the turn position, and
# will automatically cycle to the beginning of turn_order if
# turn_position is higher than the index of turn_order's last element.
# Recall that turn_position is 0 indexed.
# 
# In addition, start_next_turn keeps up to date the variables detailing
# info about the current encounter.
# 
# DEV - Description of how the game screen uses the queue system should
#   be in the class description, and a reference should be made to it
#   here.
# DOC - start_next_turn is not only queued by handle_turn_ended, but by
#   start_encounter as well.
func start_next_turn():
	# BUG - This check shouldn't be needed, but it is
	# DEV - Figure out why this check is needed, and implement something
	#  more elegant.
	if turn_order == []:
		return
	
	var next_entity
	if turn_position == turn_order.size():
		# Then turn_position is higher in index than exists in turn_order
		Global.update_log("\n"
		                +"end of turn order" + "\n"
		                +"-----------------", DEBUG)
		
		next_entity = turn_order[0]
		turn_position = 0
		total_rounds += 1
	else:
		next_entity = turn_order[turn_position]
	
	Global.update_log("\n"
	                +"starting turn " + str(turn_position) + "\n"
	                +"(" + str(next_entity.name) + ")", DEBUG)
	next_entity.start_turn()
	

# Waits ahead of queuing the appropriate event to handle a defeated
# entity. This can vary depending on whether the defeated entity is the
# player or a mob. The wait allows the game to describe what happened.
func handle_entity_defeated(entity):
	# More descriptions are inserted into a turn when an entity is
	# defeated, so pauses must be added. One pause is added at the end of
	# the turn in handle_turn_ended, so the last message does not need a
	# pause added.
	queue_wait()
	
	var message
	if entity == player:
		queue_describe("You were slain", 0)
		queue_event("show_game_over", null, 0)
	else:
		queue_describe(str(entity.entity_name) + " defeated", 0)
		if mobs.find(entity) != -1:
			remove_mob(entity)
		else:
			Global.update_log("mob defeated was never in mobs array!", WARNING)
		
		Global.update_log("\n------------------------------", DEBUG)
		Global.update_log("entity defeated: " + str(entity), DEBUG)
		Global.update_log("current turn: " + str(turn_position), DEBUG)
		Global.update_log("new mobs array: " + str(mobs), DEBUG)
		Global.update_log("new turn order: " + str(turn_order), DEBUG)
		Global.update_log("------------------------------", DEBUG)
		
		if mobs == []:
			Global.update_log("trying to end encounter", DEBUG)
			queue_event("end_encounter", null, 0)
	

# This is a hacky debugging tool. It may cause bugs.
func handle_reset_pressed():
	if active_menu and active_menu != action_menu:
		active_menu.leave_menu()
	switch_active_menu(action_menu)
	mobs = []
	
	queue_event("end_encounter", null, 0)
	if turn_position == 0:
		player.end_turn()
	

# This function will change as seen fit for debugging the game
# DEV - Create a CheatCodes class and call functions from here,
#   thereby preserving old handle_debug_pressed operations.
func handle_debug_pressed():
#	player.max_out()
	if is_active_ui:
		print(active_menu.name)
	

func handle_pause_pressed():
	open_pause_menu()
	
