extends "res://scripts/ui/list_menu.gd"

var game_screen
var player
var saved_selection

func _ready():
	game_screen = Global.get_game_screen()
	game_screen.connect("mob_selected", self, "handle_mob_selected")

func handle_item_selected(selection):
	player = game_screen.player # This cannot be done in _ready as the player wasn't yet added
	
	# Option 0 - Attack
	if   selection == 0:
		saved_selection = selection
		game_screen.open_mob_select_menu()
	
	# Option 1 - Skills
	elif selection == 1:
#		print("You selected \"spells\"")
		player.game_class.open_skills_menu()
	
	# Option 2 - Inventory
	elif selection == 2:
#		print("You selected \"items\"")
		player.open_inventory()
	

func handle_mob_selected(mob_selection):
	# The selection was the item you initially selected from this menu
	if saved_selection == 0:
		player.attack(game_screen.mobs[mob_selection])
	