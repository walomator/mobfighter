extends "res://scripts/ui/list_menu.gd"

# FEAT - Add a "next page" item

var _items_per_page

var _pages = 0
var _current_page = 0
var _paged_items = [[]]
var _current_page_items = [] # Holds final display text for labels
var _current_page_labels = [] # Not determined until draw_page is called

var origin_label

func _ready():
	# The origin label will become visible if there are no items
	origin_label = self.get_node("OriginLabel")
	if origin_label == null:
		origin_label = Label.new()
		origin_label.text = "Nothing"
		origin_label.visible = false
		self.add_child(origin_label)
		origin_label.position = cursor.position + Vector2(cursor.rect_size.x, 0)
		
	

func is_empty_menu():
	var test = false
	if _current_page_items == []:
		test = true
	return test
	

func set_items_per_page(new_items_per_page):
	_items_per_page = new_items_per_page
	

# The full_items array should be composed of STRINGS equal to the final
# text of each item.
# DEV - Using a potion in inventory_menu.gd would reduce the quantity
#   remaining. To redraw the menu with the updated displayed quantity
#   may be a pain.
func split_to_pages(full_items):
	# Check to see if there are any items to prepare to draw. An empty
	# full_items array will lead to the otherwise invisible origin_label
	# to become visible on "draw_page". 
	if full_items == null:
		_pages = 0
		_current_page = 0
		_paged_items = [[]]
		_current_page_items = []
		return
	
	elif full_items.size() <= _items_per_page:
		_pages = 1
		_current_page = 0
		_paged_items = [full_items]
		_current_page_items = full_items
		return
	
	else:
		_pages = 0 # to be incremented
		_current_page = 0
		
		# Break up full_items into pages of items, where each page is up to
		# as many items as _items_per_page, and the pages are stored in the
		# array "_paged_items". See _current_page_items for the page of items
		# (by name) that is going to be drawn.
		var num_pages_needed = ceil(full_items.size() / _items_per_page)
		for page in range(0, num_pages_needed):
			# items_this_page is necessarily equal to _items_per_page, except on
			# the last page
			var items_this_page = min(full_items.size() - (page * _items_per_page), _items_per_page)
			for item in range(0, items_this_page):
				_paged_items[page].append(item)
			# end for item
			
			_pages += 1
		# end for page
		
		# Set the current page of items to later draw or otherwise access.
		_current_page_items = _paged_items[_current_page]
	

# DEV - No args as the func should be used after simply manipulating
#   the value of "_current_page".
func draw_page():
	_current_page_labels = []
	# DEV - When flipping pages is added, be sure to pre-free the labels.
	
	# If the page is empty, then it can be assumed that there are no items
	# at all. Draw the origin_label instead.
	if _current_page_items == []:
		origin_label.visible = true
		return
	
	# The labels are drawn starting at the position of the invisible
	# origin_label, then working downwards according to the step set on
	# the cursor.
	var draw_position = origin_label.rect_position
	for item_text in _current_page_items:
		var item_label = Label.new()
		_current_page_labels.append(item_label)
		self.add_child(item_label)
		item_label.rect_position = draw_position
		item_label.set_name(item_text)
		item_label.text = item_text
		
		# Increment draw_position for next item_label
		draw_position += Vector2(0, cursor.step_length)
	
	# Update the number of items in the menu
	cursor.max_selection = _current_page_items.size() - 1
	
	# Note that the origin_label remains a hidden child of this menu that
	# is not found in _current_page_labels or _current_page_items
	