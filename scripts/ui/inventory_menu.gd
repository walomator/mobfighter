extends "res://scripts/ui/paged_menu.gd"

# DEV - Add a page scrolling feature
# DEV - Add a visible "back" option and FIX leavable bug
# DEV - Add a title

var game_screen
var player
var inventory
var saved_selection

func _ready():
	set_items_per_page(6)
	set_leavable() # BUG - Menu is still not "leaving"
	
	game_screen = Global.get_game_screen()
	player = game_screen.player
	inventory = player.inventory 
	
	# Format a display_inventory to split into pages and then draw.
	var display_inventory = []
	for item in inventory:
		# The inventory is an array of 3-element arrays. The first element is
		# the name of the item, the second element is the quantity of the
		# item. The third element is not important for display. The desired
		# menu entry for each item is "item_name xN" where xN is absent where
		# N = 1. Example: "potion x4".
		var display_item
		if item[1] == 1:
			display_item = item[0]
		else:
			display_item = item[0] + " x" + str(item[1])
		display_inventory.append(display_item)	
	
	# split_to_pages can deal with an empty array
	split_to_pages(display_inventory)
	draw_page()
	

# BUG - Game will not give control back to action_menu unless the
#   player ends their turn, meaning "You are out of everything!" must
#   rub salt in the wound and end the player's turn or game softlocks.
func handle_item_selected(selection):
	if is_empty_menu():
		game_screen.describe("You are out of everything!")
	else:
		player.use_item(selection)
	player.end_turn()
	close_menu()
	

#func handle_mob_selected(mob_selection):
#	pass
	