extends Container

# BUG - There is an undocumented engine function "set_focus_mode"
# DEV - Document variables and explain leavable, _active, etc.
#   Included in the explanation of active, mention that set_active will
#   unhide the mouse, which may not be a desired feature in some menus.
# 
# This script can be used on a node already in a scene tree with proper
# menu items and a cursor, or all can be created in code. It is best to
# create the menu in the scene tree beforehand, as most of the
# variables that make each menu unique are exported so they can be
# manipulated in the scene tree. 
# 
# Note that ALL nodes that are menus should be in group "Menus".
# 
# list_menu objects do need to be partially implemented in code,
# however. The list_menu class is designed to be inherited from, so
# that a custom script can overload the handle_item_selected function
# or other functions.
# 
# The list_menu utilizes a cursor object. This cursor is connected by
# signals to a list_menu, and the cursor is manipulated by input
# received through list_menu. The cursor also has exported variables
# that should be changed from their default values to be more useful.
# To get the item selected by the cursor, cursor.select() is used, and
# the selection number, counting from 0, is sent to
# handle_item_selected.
# 
# Receiving input in a list menu must be set up manually. The function
# "forwarded_input" can be called from another object that is receiving
# the input.

signal menu_closed
signal menu_left

var cursor_scene = preload("res://scenes/ui/Cursor.tscn")
var cursor

# The "_active" variable is set in a menu's startup, but otherwise
# "set_active()" should be used. The menu will still be set active by
# user_interface if it is the only menu.
export var _active = true
export var leavable = false


func _ready():
	if !self.is_in_group("Menus"):
		Global.update_log("Menu \"" + self.name + "\" is not in Menus group!", Global.ERROR)
	
	# Find the child in group "Cursors"
	for child in get_children():
		if child.is_in_group("Cursors"):
			cursor = child
			break
	if cursor == null:
		_create_cursor()
	cursor.connect("item_selected", self, "handle_item_selected")
	
	# Calls the setters since they may have more manipulation to do based
	# on the value of the exported variable.
	if _active:
		set_active()
	else:
		set_inactive()
	
	if leavable:
		set_leavable()
	else:
		set_unleavable()
	

# Custom menus that overload this function (such as irregular_menu)
# will need to accomodate the mouse in their own way. There is no
# realized good default behavior for deviations from list_menu with
# respect to mouse movement.
func forwarded_input(event):
#	print("input event received in: " + str(self)) # Debug message
	if cursor.locked == false:
		if event.is_action_pressed("ui_up"):
			cursor.decrement_selection()
		elif event.is_action_pressed("ui_down"):
			cursor.increment_selection()
		
		# Mouse movement turns the cursor to a free-moving mouse. Re-snapped
		# on next cursor.draw(), which is called with cursor.increment_ or
		# decrement_selection.
		elif event is InputEventMouseMotion:
			cursor.unsnap()
			cursor.rect_global_position = event.get_position()
		elif cursor.snapped:
			if event.is_action_pressed("ui_right") or event.is_action_pressed("ui_accept"):
				cursor.select()
		elif !cursor.snapped:
			if event.is_action_pressed("ui_accept") or event.is_action_pressed("mouse_select"):
				cursor.mouse_select()
		# FEAT - Need a leave option for mouse users
		elif leavable and event.is_action_pressed("ui_left"):
			leave_menu()
	# End if cursor.locked == false
	

# DEV - Possible function for handling mouse movement
func mouse_input(mouse_event):
	pass
	

# DEV - Possible function for handling touch movement
func touch_input(touch_event):
	pass
	

func _create_cursor():
	cursor = cursor_scene.instance()
	cursor.add_to_group("Cursors")
	self.add_child(cursor)
	return cursor
	

# This function should be used by the UI. It does not on its own fully
# set the active menu.
# The variable "_active" may be true before this function is called.
func set_active():
	_active = true
	if cursor.config["remember"] == false:
		cursor.selection = 0
	# Unhide the mouse when active.
	if !cursor.snapped:
		cursor.visible = true
	

func set_inactive():
	_active = false
	# Hide the mouse, since it will not respond to mouse movement.
	if !cursor.snapped:
		cursor.visible = false
	

func is_active():
	return _active


func set_leavable():
	leavable = true
	

func set_unleavable():
	leavable = false
	

func is_leavable():
	return leavable
	

# Sends a signal that the menu is closing and calls queue_free(). The
# UI should be set to receive the "menu_closed" signal. The UI should
# also handle switching the active_menu immediately so that
# forwarded_input is not called on this previously freed node.
# Unlike leave_menu(), this function is only used externally, so
# whether close_menu() should be allowed to be called must be managed
# externally.
func close_menu():
#	print("signal: menu_closed from: " + str(self)) # Debug message	
	emit_signal("menu_closed", self)
	self.queue_free()
	

# The outcome of leave_menu() is similar to close_menu() when the UI is
# set to receive the "menu_left" signal. The difference is that the
# menu is not freed, so it can be opened again.
# 
# This function does not check if "leavable" is true before exiting.
func leave_menu():
#	print("signal: menu_left from: " + str(self)) # Debug message
	emit_signal("menu_left", self)
	

# Handles the outcome of the cursor's selection. Since this is menu-
# specific, this function should be overloaded with a custom script
# inheriting this class.
func handle_item_selected(selection):
	print("Error: no handler for selection: " + str(selection)) # Debug message
	