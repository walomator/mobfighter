extends "res://scripts/ui/cursor.gd"

# max_selection, used by parent class cursor.gd, is instead set
# dynamically based on the length of irregular_positions and
# step_length is not used.

var irregular_positions

func _ready():
	config["remember"] = false
	

# This should be called prior to calling draw(). If the
# irregular_positions have been changed, it is probably about to be set
# active again. You can overload the set_active function and draw there
# while managing "selection" according to config["remember"].
func set_irregular_positions(define_irregular_positions):
	irregular_positions = define_irregular_positions
	
	max_selection = len(irregular_positions) - 1
	if selection > max_selection: # DEV - Consider using a setter
		selection = max_selection
	

func draw():
	rect_position = irregular_positions[selection]
	