extends Control

# DEV - Document this function.

signal ui_closed
signal ui_left
signal pause_timer_ended

enum {ACTIVE, INACTIVE} # Used in create_menu for active/inactive menus

# External scripts
var UserInterface = get_script().get_base_script() # Scripts cannot load themselves with preload
const ListMenu = preload("res://scripts/ui/list_menu.gd")
const SimpleTimer = preload("res://scripts/simple_timer.gd")

# A solid-colored background defined by Global
# FEAT - Support more intricate backgrounds
var background

# All nodes that are menus should be in group "Menus"
var menus = []

# syslog holds every message sent to "update_log" along with the
# corresponding priority in an array of 2 element arrays.
var syslog = []

# Like menus, UIs can be set active. This way, if multiple UIs are
# open, you can switch between them without closing them.
# DEV - With too many sub-UIs, this var will be hard to keep track of
# DEV - Functions that check booleans should start with "is_", but
#   variables generally should not
var is_active_ui = true

# The menu that will get control when the scene is opened. To set it as
# the active menu, which is not done automatically, call
# switch_active_menu(default_menu)
var default_menu

# Set this to the name of the node in the scene tree that should be
# the default menu. Otherwise the first node found in group "Menus"
# will be default.
export var default_menu_name = "DefaultMenu"

# The active menu, which will receive input from the user. If the
# active menu is deactivated, it will be stored in default_menu.
var active_menu

# This holds the active menu when active menus are disabled, for
# example, when pausing.
var saved_active_menu

# Another UI object that can be created on top of the current UI
var sub_ui

# Used by pause() when the game is paused. When paused, if the active
# scene is switched, the new active scene won't become active until the
# game is unpaused with unpause().
var paused = false

var pause_timers = []

# This _ready() function tries to find all menus in the UI and set an
# active_menu if the menu should be active. If this node has one or
# more menus, the default_menu will be set to the menu with node name
# matching default_menu_name. The menu must also be in group "Menus".
func _ready():
#	print("creating user interface: " + str(self)) # Debug message
	
	# Find all menus in the scene
	for child in get_children():
		if child.is_in_group("Menus"):
			menus.append(child)
			child.connect("menu_closed", self, "handle_menu_closed")
	
	# Set a default menu if there are any menus
	for menu in menus:
		if menu.name == default_menu_name:
			default_menu = get_node(default_menu_name)
	
	if not default_menu and menus:
		default_menu = menus[0]
	
	if default_menu:
		if default_menu.is_active():
			switch_active_menu(default_menu)
	

func forwarded_input(event):
	if is_active_ui:
		if active_menu:
			active_menu.forwarded_input(event)
	elif sub_ui:
		if sub_ui.is_active_ui:
			sub_ui.forwarded_input(event)
	

# DEV - Deprecated. Use Global.update_log instead, or else the
#   update_log will be filled with the warnings below.
enum {ALERT = 1, ERROR = 3, WARNING = 4, INFO = 6, DEBUG = 7}
func update_log(message, priority = Global.INFO):
	Global.update_log(message, priority)
	Global.update_log("^Message was sent to user_interface.update_log, use Global.update_log instead.", Global.WARNING)

func pause():
	paused = true
	disable_active_menu()
	

func unpause():
	paused = false
	enable_active_menu()
	

# pause_for
# Args - time: in seconds
#      - initiator: for optional use with callback parameter, the
#          caller of the function
#      - callback: the function belonging to initiator to call after
#          the pause_timer runs out
# 
# Initiates pause for a set time, then unpauses. Calling this function
# when there is already a timed pause will add a new timer. The UI will
# remain paused until ALL timers are complete, but each timer will
# trigger a callback function when it is complete.
#
# There are two chronological events that occur when the pause_timer
# finishes. First, the pause_timer object will call handle_timeout.
# Second, if there was an initiator and callback passed, the timer will
# also trigger the callback function. Return arguments are supported in
# version Alpha 0.0.2 of MobFighter, but the feature set is currently
# unstable and may be deprecated.
# 
func pause_for(time, initiator = null, callback = null):
	pause()
	var pause_timer = SimpleTimer.new()
	pause_timers.append(pause_timer)
	
	if initiator and callback:
#		print("connecting \"unpause\" to: " + str(initiator))
#		print("  with callback: " + str(callback))
		pause_timer.add_connection(initiator, callback)
	
	pause_timer.start(self, "unpause", time)
	

func set_background(color):
	if !background:
		background = ColorRect.new()
		background.name = "Background"
		background.show_behind_parent = true
		background.color = color
		self.add_child(background)
		background.rect_size = Global.resolution
	else:
		background.color = color
	

# Functions that begin with "create", such as this one, will always
# create an object that the function describes and pass the object
# as a return parameter.
func create_menu(mode = ACTIVE, menu_scene = null):
	var new_menu
	if menu_scene:
		new_menu = menu_scene.instance()
	else:
		new_menu = ListMenu.new()
	
	if mode == ACTIVE:
		new_menu._active = true
	else:
		new_menu._active = false
		
	self.add_child(new_menu)
#	print("connected for signals: " + str(new_menu)) # Debug message
	new_menu.connect("menu_closed", self, "handle_menu_closed")
	new_menu.connect("menu_left", self, "handle_menu_left")
	
	if mode == ACTIVE:
		switch_active_menu(new_menu)
	
	menus.append(new_menu)
	return new_menu
	

# Args- new_menu: new active menu node (must already be in scene tree)
# 
# This function can switch one active menu for another, or if new_menu
# is a newly created menu that is set_active() as part of its _ready(),
# this function can make the user_interface track it as active without
# calling set_active() on the new_menu again.
# 
# If "paused" is true, then switch_active_menu modifies the
# saved_active_menu, which keeps for the moment all menus inactive.
func switch_active_menu(new_menu):
#	print("switched active_menu to: " + str(new_menu)) # Debug message
	
	# This function is used in init when there is no active_menu, so the
	# existance of active_menu should be verified
	if active_menu:
		active_menu.set_inactive()
	
	if paused:
		saved_active_menu = new_menu
	else:
		active_menu = new_menu
#		if active_menu._active == false:
#			active_menu.set_active()
#		# Else new_menu was a brand new menu, and was already set active but
#		# not tracked as such
		active_menu.set_active()
	

# Enables the active menu, which if previously existing is stored in
# saved_active_menu.
func enable_active_menu():
	if saved_active_menu:
		switch_active_menu(saved_active_menu)
	

# Disables the current active menu. It is only necessary to do this
# when no active menu is desired.
func disable_active_menu():
	if !active_menu:
		return
	active_menu.set_inactive()
	saved_active_menu = active_menu
	active_menu = null
	

# Creates a user interface overlaying the current one. Control is passed back
# to this UI when the sub-UI calls close_ui(). Memory management for the closed
# sub-UI is dealt with in handle_sub_ui_closed.
func create_sub_ui(ui_scene = null):
	disable_active_menu()
	self.is_active_ui = false
	
	if ui_scene:
		sub_ui = ui_scene.instance()
	else:
		sub_ui = UserInterface.new()
	
	self.add_child(sub_ui)
	sub_ui.set_background(Global.palette[0])
	sub_ui.connect("ui_closed", self, "handle_sub_ui_closed")
	sub_ui.connect("ui_left", self, "handle_sub_ui_left")
	
	return sub_ui
	

func switch_active_ui(new_ui):
	if new_ui == sub_ui:
		self.is_active_ui = false
	elif new_ui == self:
		sub_ui.is_active_ui = false
	new_ui.is_active_ui = true
	

func switch_to_active_ui():
	self.is_active_ui = true
	sub_ui.is_active_ui = false
	

# Simply emits a signal. This should preferably happen when this node is a sub-
# UI for another UI. That UI will clear this UI from memory (see
# handle_sub_ui_closed). According to the default behavior of
# handle_menu_closed, if all menus are closed, the UI will close. If this UI is
# not a sub-UI, the signal "ui_closed" may do nothing.
func close_ui():
#	print("signal: ui_closed from: " + str(self)) # Debug message
	emit_signal("ui_closed")
	

func leave_ui():
	pass
	emit_signal("ui_left")
	

# This menu closing handler will also close the UI if all menus are closed. If
# that behavior is not desired, overload this function.
func handle_menu_closed(closed_menu):
#	print("menu closed: " + str(closed_menu)) # Debug message
	menus.erase(closed_menu)
	if menus == []:
		close_ui() # DEV - This is a temporary solution
#	else: # Debug message
#		print("menu closed but there are remaining menus: " + str(menus)) # Debug message
	

# Overload with a UI's particular implementation.
# DEV - Implement default behavior if necessary.
func handle_menu_left(left_menu):
	pass
	

func handle_sub_ui_closed():
#	print("ui closed: " + str(sub_ui)) # Debug message
	sub_ui.queue_free()
	sub_ui = null # sub_ui is otherwise recognized as "previously freed instance"
	
	# DEV - With too many sub-UIs, this var will be hard to keep track of.
	self.is_active_ui = true
	

# DEV - Document this function
func handle_sub_ui_left(left_ui):
	left_ui.is_active_ui = false
	self.is_active_ui = true
	

func handle_pause_timer_finished(pause_timer):
	pause_timers.erase(pause_timer)
	if pause_timers == []:
		unpause()
	

func handle_timeout(timer, timer_name):
	if timer_name == "unpause":
		handle_pause_timer_finished(timer)
	
	timer.queue_free()
	
