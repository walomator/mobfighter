extends "res://scripts/ui/irregular_menu.gd"

# DEV - Document this class, including why functions were overloaded.
# 
signal mob_selected

func forwarded_input(event):
	if cursor.locked == false:
		if event.is_action_pressed("ui_right"):
			cursor.increment_selection()
		elif event.is_action_pressed("ui_left"):
			cursor.decrement_selection()
		elif event.is_action_pressed("ui_accept"):
			cursor.select()
		
		elif leavable:
			if event.is_action_pressed("ui_up") or event.is_action_pressed("ui_down"):
				leave_menu()
	

func _create_cursor():
	var cursor_scene = load("res://scenes/ui/IrregularCursor.tscn")
	cursor = cursor_scene.instance()
#	cursor.set_irregular_positions(Vector2(0, 0))
	cursor.add_to_group("Cursors")
	self.add_child(cursor)
	return cursor
	

func set_active():
	_active = true
	if cursor.config["remember"] == false:
		cursor.selection = 0
	elif cursor.selection > cursor.max_selection:
		cursor.selection = cursor.max_selection
	
	# The cursor has not been oriented correctly since it was last
	# drawn with a different set of irregular_positions 
	cursor.draw()
	cursor.visible = true
	

func set_inactive():
	_active = false
	cursor.visible = false
	

func set_irregular_positions(define_irregular_positions):
	cursor.set_irregular_positions(define_irregular_positions)
	

func handle_item_selected(selection):
#	print("signal: mob_selected from: " + str(self)) # Debug message
	
	# leave_menu must be called first or the turn ends
	# DEV - Must it? I've swapped the order and nothing happened.
	emit_signal("mob_selected", selection)
	leave_menu()
	
