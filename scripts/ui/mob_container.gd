extends HBoxContainer

# Mob containers align monster textures (as TextureRect) into the
# MobArea node. When a mob is defeated, the texture will disappear but
# this object remains behind to keep the alignment with other mobs and
# to potentially be refilled with a new mob mid-battle.
# 
# To find an orphaned container, check if the texture_rect property
# returns null.

signal mob_defeated

var texture_rect
var v_container

func _ready():
	v_container = get_node("VBoxContainer")


# FEAT - If a mob's script gives a filename that doesn't exist, this
#   class should replace it with missingmob.png
func add_texture(texture):
	if texture_rect:
		return
	texture_rect = TextureRect.new()
	texture_rect.texture = texture
	
	v_container.add_child(texture_rect)
	

# Clears the object and removes the reference to it
func remove_texture():
	texture_rect.queue_free()
	texture_rect = null
	


func clear_area():
	self.queue_free()
