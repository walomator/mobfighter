extends TextureRect

# DEV - Is "locked" needed if menus are either active or inactive?
# Rarely, but it may be out of the scope of this project.
# 
# A cursor is an object that is used by menus. It receives directions
# in the form of increment_selection and decrement_selection from
# list_menu. This changes the position of the cursor based on
# step_length, and changes the selection number by 1. When select() is
# called from list_menu, the cursor will emit a signal to provide the
# selection number, which counts starting from 0.
# 
# You can build a menu in the scene editor. Make the cursor a child of
# the list_menu object. The cursor's _ready will likely crash the game
# if its parent is not a menu.
# 
# Note that ALL nodes that are cursors should be in group "Cursors".
# 
# In the case where list_menu.gd creates a cursor with _create_cursor,
# the cursor is instanced, added to the Cursors group, then added to
# the scene.

signal item_selected

export var selection = 0 # Lowest position should always be zero
export var max_selection = 0
export var step_length = 1
export var snapped = true # DEV - Should this/does this work?

var parent_menu
var sword_texture = preload("res://textures/ui/cursor.png")
var mouse_texture = preload("res://textures/ui/cursor2.png")
var starting_position = Vector2(0, 0) 

var locked = false
# DEV - Make an export variable
var config = {"remember":true, "cycle":true}

func _ready():
	parent_menu = get_parent()
	
	if !parent_menu.is_in_group("Menus"):
		Global.update_log("Cursor " + str(self) + " is being used by a non-menu!" + "\n"
		                 +"This can result in a hard crash!", Global.ALERT)
	
	if !self.is_in_group("Cursors"):
		Global.update_log("Cursor from \"" + parent_menu.name + "\" is not in Cursors group!", Global.ERROR)
	
#	print("creating cursor: " + str(self))
	starting_position = rect_position
	

func increment_selection():
	selection += 1
	if selection > max_selection:
		if config["cycle"] == true:
			selection = 0
		else:
			selection = max_selection
	draw()
	

func decrement_selection():
	selection -= 1
	if selection < 0:
		if config["cycle"] == true:
			selection = max_selection
		else:
			selection = 0
	draw()
	

#func set_max_selection(new_max_selection):
#	if new_max_selection < 0:
#		return
#	if new_max_selection > selection:
#		selection = new_max_selection
#	max_selection = new_max_selection
#	

# This "draw" is snappy to the menu items. It is not called when the
# cursor is controlled by the mouse.
func draw():
	self.texture = sword_texture
	snapped = true
	rect_position.x = starting_position.x
	rect_position.y = starting_position.y + (selection * step_length)
	

# Unsnaps from the regular positioning (probably by a mouse movement).
# The function "draw" is essentially a snap function to undo the
# unsnap.
func unsnap():
	self.texture = mouse_texture
	snapped = false
	

func select():
	emit_signal("item_selected", selection)
	

# FEAT - Add real borders to buttons instead of referencing margins so
#   users can see where to click clearly.
func mouse_select():
	for item in parent_menu.get_children():
		if self.is_overlaying(item):
			selection = floor((rect_position.y - starting_position.y)/step_length)
			emit_signal("item_selected", selection)
			break
	

# The passed argument must be a node that inherits Control
func is_overlaying(node):
	var test = false
	if !node.is_class("Control"): # Classes that inherit Control also pass
		Global.update_log("func cursor.is_in_bounds() was passed a non-control based node", Global.ERROR)
		return false
	var x = self.rect_position.x
	var y = self.rect_position.y
	if (y > node.margin_top) and (y < node.margin_bottom) and (x > node.margin_left) and (y < node.margin_bottom):
		test = true
	return test
	