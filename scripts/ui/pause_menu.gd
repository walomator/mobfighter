extends "res://scripts/ui/list_menu.gd"

var game_screen
var player

func handle_item_selected(selection):
	# BUG - Softlocks the game
	if   selection == 0:
		close_menu()
	
	# DEV - No confirmation on quitting! Although there is no save either.
	elif selection == 1:
		Global.restart()
		close_menu()
	
	# DEV - No confirmation on quitting! Although there is no save either.
	elif selection == 2:
		Global.shutdown()
	