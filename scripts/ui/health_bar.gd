extends Node2D

# DEV - Would using a TextureProcess be just as fulfilling?

# The node used to display the points in text form
var point_counter

# This value should match to the number of pieces of the health bar
export var MAX_FILL_PIECES = 10
var fill_pieces = MAX_FILL_PIECES

# This prefix can add units to prepend the point value on the point
# counter e.g. "HP: "
export var point_prefix = ""

# Setting the fill_points will automatically set the fill of the health
# bar proportionally based on the max_fill_points
var max_fill_points = 1
var fill_points = 1

func _ready():
	point_counter = get_node("PointCounter")
	point_redraw()
	

func set_fill_pieces(amount):
	if amount == fill_pieces:
		return
	if amount < 0:
		amount = 0
	if amount > MAX_FILL_PIECES:
		amount = MAX_FILL_PIECES
	fill_pieces = amount
	redraw()
	

func set_max_fill_points(amount):
	if amount > 0:
		max_fill_points = amount
	point_redraw()
	

func set_fill_points(amount):
	if amount > max_fill_points:
		fill_points = max_fill_points
	else:
		fill_points = amount
	
	point_redraw()
	

func set_point_prefix(name):
	point_prefix = name
	counter_redraw()
	

func counter_redraw():
	if point_counter:
		point_counter.text = point_prefix + str(fill_points)
	

func point_redraw():
	counter_redraw()
	
	var fraction = fill_points / max_fill_points
	fill_pieces = round(fraction * MAX_FILL_PIECES)
	redraw()
	

func redraw():
	if fill_pieces >= 1:
		for i in range(1, MAX_FILL_PIECES + 1):
			if i <= fill_pieces:
				get_node("Piece" + str(i)).visible = true
			else:
				get_node("Piece" + str(i)).visible = false
	

func get_fill_pieces():
	return fill_pieces
	