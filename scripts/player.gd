extends "res://scripts/entity.gd"

# player.gd holds functions which forward to the corresponding
# function in the player's game_class object. This allows classes to
# have unique ways of handling common functions, or default to the
# parent game_class's default_* version of the functions for the most
# common implementation of that function. It also means players can
# theoretically change classes.
# 
# player.gd has the stats of an entity, but the stat assignment is done
# through its game_class.

enum {GLADIATOR, ROGUE, MAGE, WARLOCK, PRIEST}
const Userinterface = preload("res://scripts/ui/user_interface.gd")
const GameClass = preload("res://scripts/game_classes/game_class.gd")
const GladiatorClass = preload("res://scripts/game_classes/gladiator/gladiator_class.gd")
const MageClass = preload("res://scripts/game_classes/mage/mage_class.gd")

var inventory_menu_scene = preload("res://scenes/ui/InventoryMenu.tscn")

var game_class
var inventory
var _status_effect_iteration

func _ready():
	# The game_class will do stat assignment
	# FEAT - this should not be hard-coded
	game_class = GladiatorClass.new(self)
	set_name(game_class.class_name)
	
	# DEV - The inventory array is a WIP implementation, and current
	#   values are set for testing purposes.
	inventory = [["Potion", 3, null], ["Ether", 1, null]]
	

# At the start of the player's turn, game_screen will do nothing, and
# the turn ends due to player input. While nothing is happening at the
# start of the player's turn, i.e., game_screen is no longer using
# "pause_for", the user can control the menu.
# 
# DEV - Consider handling status effects here at the start of the turn
#   rather than putting status effects in the turn order.
# DEV - A new paradigm for handling user control is being considered.
#   See "game_screen.gd".
func start_turn():
	pass
	

func end_turn():
	emit_signal("turn_ended", self)
	

# Overloads the set_hp function so that the health bar stays up to date
func set_hp(amount):
	if amount == hp:
		return
	
	hp = round(amount)
	if hp <= 0:
		emit_signal("entity_defeated", self)
	elif hp > max_hp:
		hp = max_hp
	
	ui.set_health_bar(hp)
	

# Notice the leading underscore. IGNORES STATUS EFFECTS and reduces HP.
func _decrease_hp(amount):
	set_hp(get_hp() - amount)
#	ui.set_health_bar(get_hp())
	

# Overloads entity.gd's decrease_hp func with its
# _adjust_and_decrease_hp func. This provides the maximum likelihood
# for considering damage-resisting status effects.
func decrease_hp(amount):
	_adjust_and_decrease_hp(amount)
	

func attack(mob):
	game_class.attack(mob)
	

func use_item(selection):
	var item = inventory[selection]
	ui.describe("You use \"" + item[0] + "\"")
	item[1] -= 1
	if item[1] == 0:
		inventory.remove(selection)
	

# DEV - Consider making this a generic class function, just in case a
#   class that couldn't use items was desired.
func open_inventory():
	var sub_ui = ui.create_sub_ui()
	sub_ui.create_menu(sub_ui.ACTIVE, inventory_menu_scene)
	

# Overloads entity.gd's "_initiate_spell" to add descriptions for
# success or failure
func _initiate_spell(spell_name):
	var success = false
	var spell = spellbook.get_spell(spell_name)
	if spell:
		if get_mp() < spell.COST:
			ui.describe("Out of MP")
		else:
			success = true
			decrease_mp(spell.COST)
			ui.describe("You cast \"" + spell_name + "\"")
	return success
	