extends Node

"""
This holds all spells used by the base game. You can inherit this
class to grant more spells in a mod, but the spellbook should not be
divided among core classes, for simplicity's sake. Skills and mob
abilities should be implemented here too.

Use _ititiate_spell func in entity.gd to test whether it is OK to
cast at its cost.

Variables:
_known_spells
  An array of spell names. Ignores the value of knows_all_spells.

knows_all_spells
  This makes the func knows_spell always return true. It does not
  change the _known_spells array, which will remain finite.
"""

var _known_spells = []
var knows_all_spells = false

func learn_spell(spell_name):
	if !(spell_name in _known_spells):
		_known_spells.append(spell_name)
		_known_spells.sort() # DEV - I don't know into what order this goes
	

func learn_spells(spells_array):
	for spell_name in spells_array:
		learn_spell(spell_name)
	

func forget_spell(spell_name):
	_known_spells.erase(spell_name)
	

func forget_spells(spells_array):
	for spell_name in spells_array:
		forget_spell(spell_name)
	

func get_known_spells():
	return _known_spells
	

func knows_spell(spell_name):
	if knows_all_spells:
		return true
	else:
		if spell_name in _known_spells:
			return true
	return false
	

# DEV - Can be used to get any object owned by this class
func get_spell(spell_name):
	var spell_class = self.get(spell_name)
	return spell_class
	

# Spell classes
class ArcaneMissile:
	const COST = 1
	static func cast():
		pass
	

class Barrier:
	const COST = 1
	static func cast():
		pass
	

class Blight:
	const COST = 1
	static func cast():
		pass
	

class ChaosBolt:
	const COST = 1
	static func cast():
		pass
	

class Curse:
	const COST = 1
	static func cast():
		pass
	

class DarkTouch:
	const COST = 1
	static func cast():
		pass
	

class Fire:
	extends Object
	const COST = 1
	
	static func cast(caster, varargs):
		var target = varargs[0]
		var power = 1
		if len(varargs) == 2:
			power = varargs[1]
		
		var damage = (Global.randint(1, 2))^power
		target.decrease_hp(damage)
	

class FireAndFlames:
	const COST = 1
	

# FEAT - Use power-scaling on this and Ice
class Flames:
	const COST = 3
	
	static func cast(caster, varargs):
		var target = varargs[0]
		
		var damage = Global.randint(3, 10) + floor(caster.get_foc() / 2)
		target.decrease_hp(damage)
		
	

class Heal:
	const COST = 1
	static func cast():
		pass
	

class HolyLight:
	const COST = 1
	static func cast():
		pass
	

class Ice:
	const COST = 3
	
	static func cast(caster, varargs):
		var target = varargs[0]
		
		var damage = Global.randint(6, 7) + floor(caster.get_foc() / 2)
		target.decrease_hp(damage)
		
	

class Javelin:
	const COST = 0
	static func cast():
		pass
	

class PlaneShift:
	const COST = 1
	static func cast():
		pass
	

class Poison:
	const COST = 1
	static func cast():
		pass
	

class Shrink:
	const COST = 1
	static func cast():
		pass
	