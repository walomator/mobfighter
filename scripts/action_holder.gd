extends Node

# DEV - Use signals to be caught by a combo system
# 
# This class acts like an array but with extensibility in place. It can
# already store history and compare it to a sequence. This can be
# incorporated into a simple combo system.
# 
# Specifically, this class looks for the beginning of a new action. An
# "action" is a discrete input. This class can parse all input and only
# take valid inputs as actions. For example, mouse movement would not
# be appropriate for logging.
# 
# This class keeps a log of actions up to size HISTORY_SIZE. If the
# history size is reached, a signal will emit just before the history
# is cleared. To prevent history cleaning, consider having the object
# managing grab the history array and appending it to a longer array.

signal action_history_filled
signal sequence_entered # DEV - Could be defined by inherited class

const HISTORY_SIZE = 256
var valid_actions = ["right", "left", "up", "down"]
var history = []

func _init():
	history.resize(HISTORY_SIZE)
	

func add(action):
	if (action in valid_actions) and not (action in active_actions):
		active_actions.append(action)
		update_history(action)
	

func update_history(action):
	history.append(action)
	history.pop_front()
	

func remove(action):
	if action in valid_actions:
		active_actions.erase(action)
	

func clear():
	active_actions.clear()
	

func clear_history():
	history.clear()
	history.resize(HISTORY_SIZE)
	

func _shorten_front(array, length):
	for i in range(length):
		array.pop_front()
	return array.duplicate()
	

func get_actions(): # DEV - Deprecated
	return active_actions
	

# Compares a sequence with the recent history of equal length
func history_equals(sequence):
	var sequence_size = sequence.size()
	var recent_history
	
	if     sequence_size > HISTORY_SIZE:
		return false
	elif   sequence_size < HISTORY_SIZE:
		recent_history = _shorten_front(history.duplicate(), HISTORY_SIZE - sequence_size)
	else: #sequence_size == HISTORY_SIZE
		recent_history = history.duplicate()
	
	if sequence == recent_history:
		return true
	return false
	