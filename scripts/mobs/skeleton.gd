extends "res://scripts/mobs/mob_entity.gd"

func _init():
	texture_filename = "skeleton.png"
	

func _ready():
	set_name("Skeleton")
	set_level(1)
	set_max_hp(8 + Global.randint(3, 6))
	set_max_mp(10)
	max_out()
	# Totalling 3 points
	set_strn(1)
	set_con(1)
	set_dex(1)
	set_foc(0)
	set_wis(0)
	set_intl(0)
	

func start_turn():
	var action = Global.randint(0, 2)
	if action == 0:
		attack(ui.player)
	elif action == 1:
		spit_poison(ui.player)
	elif action == 2:
		idle()
	

func attack(player):
	describe_default_attack()
	
	var damage = Global.randint(2, 3) + floor(get_strn() / 2)
	player.decrease_hp(damage)
	end_turn()
	

func spit_poison(player):
	ui.describe(get_name() + " spews poison")
	player.add_effect("poison")
	end_turn()
	

func idle():
	ui.describe(get_name() + " rattles its bones")
	end_turn()