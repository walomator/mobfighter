extends "res://scripts/mobs/mob_entity.gd"

func _init():
	texture_filename = "goblin.png"
	

func _ready():
	set_name("Goblin")
	set_level(1)
	set_max_hp(4 + Global.randint(3, 7))
	set_max_mp(0)
	max_out()
	# Totalling 10 points
	set_strn(2)
	set_con(0)
	set_dex(3)
	set_foc(2)
	set_wis(0)
	set_intl(3)
	

func start_turn():
	attack(Global.get_game_screen().player)
	

func attack(player):
	describe_default_attack()
	
	var damage = Global.randint(0, 1) + floor(get_strn() / 2)
	player.decrease_hp(damage)
	end_turn()
	