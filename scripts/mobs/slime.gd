extends "res://scripts/mobs/mob_entity.gd"

func _init():
	texture_filename = "slime.png"


func _ready():
	set_name("Slime")
	set_level(1)
	set_max_hp(5)
	set_max_mp(20)
	max_out()
	# Totalling 10 points
	set_strn(2)
	set_con(5)
	set_dex(1)
	set_foc(1)
	set_wis(0)
	set_intl(1)


func start_turn():
	var action = Global.randint(0, 1)
	if action == 0:
		attack(ui.player)
	elif action == 1:
		sleep()


func attack(player):
	describe_default_attack()

	var damage = Global.randint(0, 1) + floor(get_strn() / 2)
	player.decrease_hp(damage)
	end_turn()
	

func sleep():
	ui.describe(get_name() + " looks drowsy")
	end_turn()
	