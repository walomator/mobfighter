extends "res://scripts/entity.gd"

# DEV - Document this class further, especially how mob_containers
#   work.
# 
# Ultimately, mob_entities and the player should be identical except in
# how they are presented in game_screen and how they are controlled. So
# in theory a player could have a "goblin" class (although this would
# require duplicating the code).
# 
# Therefore, there should not be any special mob-only features like
# having a custom spellbook. And finally, there could in theory be a
# remote_player.gd script that inherits mob_entity.

var texture_filename = "missingmob.png"
var mob_container

func _ready():
	mob_container = ui.create_mob_container(texture_filename)
	

# Overload with a mob's particular implementation. Whatever it does, it
# should always end with "end_turn". The game_screen will handle adding
# the pauses.
func start_turn():
	ui.describe("The " + entity_name + " is looking menacing")
	end_turn()
	

func end_turn():
#	print("emitting signal: turn_ended")
	emit_signal("turn_ended", self)
	

# IGNORES STATUS EFFECTS and reduces HP.
func _decrease_hp(amount):
	set_hp(get_hp() - amount)
	

# Reduces HP. Will be modified by status effects.
func decrease_hp(amount):
	_adjust_and_decrease_hp(amount)
	

func describe_default_attack():
	ui.queue_describe(get_name() + " attacks")
	

func _initiate_spell(spell_name):
	var success = false
	var spell = spellbook.get_spell(spell_name)
	if spell:
		if get_mp() < spell.COST:
			ui.describe(get_name() + "'s spell fails!")
		else:
			success = true
			decrease_mp(spell.COST)
			ui.describe(get_name() + " casts \"" + spell_name + "\"")
	return success
	

func die():
	# Leave an orphaned container to keep alignment
	mob_container.remove_texture()
	self.queue_free()
	