extends "res://scripts/mobs/mob_entity.gd"

var action_cycle = 0
const SPELL_POWER = 1

func _init():
	texture_filename = "fat_wizard.png"


func _ready():
	set_name("Fat Wizard")
	set_level(1)
#	set_max_hp(4)
	set_max_hp(12)
	set_max_mp(1)
	max_out()
	# Totalling 12 points
	set_strn(3)
	set_con(1)
	set_dex(1)
	set_foc(2)
	set_wis(1)
	set_intl(2)
	spellbook.learn_spells(["Fire"])


func start_turn():
	if action_cycle > 2:
		action_cycle = 0

	if action_cycle < 2:
#		attack(ui.player)
		cast_spell("Fire", [ui.player, SPELL_POWER])
	elif action_cycle == 2:
		attack(ui.player)
#		cast_spell("Fire", [SPELL_POWER])

	action_cycle += 1


func attack(player):
	describe_default_attack()

	var damage = Global.randint(0, 1) + floor(get_foc() / 2)
	player.decrease_hp(damage)
	end_turn()
