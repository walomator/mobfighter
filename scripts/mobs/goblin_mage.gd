extends "res://scripts/mobs/mob_entity.gd"

func _init():
	texture_filename = "goblinm.png"
	

func _ready():
	set_name("Goblin Mage")
	set_level(1)
	set_max_hp(3 + Global.randint(3, 6))
	set_max_mp(7)
	max_out()
	# Totalling 10 points
	set_strn(0)
	set_con(0)
	set_dex(2)
	set_foc(3)
	set_wis(1)
	set_intl(4)
	

func start_turn():
	attack(Global.get_game_screen().player)
	

func attack(player):
	describe_default_attack()
	
	var damage = Global.randint(0, 1) + floor(get_strn() / 2)
	player.decrease_hp(damage)
	end_turn()