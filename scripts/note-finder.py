#!/usr/bin/python3
from pathlib import Path
import sys
from sys import argv

# Use this script to find BUG, DEV, AND FEAT notes. Must pass the name
# of the note as an argument. So, e.g. "./note_finder.py DEV".
# 
# FEAT - Allow setting a custom path to check a subset of the scripts
#   for flags
# FEAT - Allow searching only what has been added to staging in git

class colors:
    BLUE = "\033[94m"
    GREEN = "\033[92m"
    RED = "\033[91m"
    ENDCOLOR = "\033[0m"

def main():
    # Exit if no arguments passed
    if len(argv) == 1:
        print("Must pass the name of the note as an argument. So, e.g.")
        print("$ " + argv[0] + " DEV")
        sys.exit(1)
    
    flags = [] # Holds the signifier words like "DEV" etc.
    recognized_flags = ["BUG", "DEV", "FEAT"]
    
    # Evaluate the arguments passed. argv would otherwise give this
    # script as the first argument, and that is no longer needed, so it
    # is deleted.
    argv.pop(0)
    if argv[0] == "ALL":
        flags = recognized_flags
    else:
        for arg in argv:
            flags.append(arg)
    
    cwd = Path(".") # current working directory
    notes = [] # The eventual displayed output
    printed_script_path = False # DEV - Not yet implemented
    
    # Sort evaluation by flag first, then file, so flags are more
    # unified in the output than files
    for flag in flags:
        flag_occurances = 0
        
        # Print the current flag
        print(colors.RED + "Looking for " + flag + "s"
             +colors.ENDCOLOR)

        # ** in cwd.glob indicates searching current and all sub-
        # directories
        for script_path in cwd.glob("**/*.gd"): 
            script = script_path.open("r") # Open in read-only mode
            lines = script.readlines()
            
            new_notes, new_occurances = scan_lines(lines, flag)
            if new_notes:
                # Print the current script's name
                print(colors.BLUE + str(script_path) + " (Occurances: "
                     +str(new_occurances) + ")" + colors.ENDCOLOR)
                for note in new_notes:
                    # Print the note without the annoying extra newline
                    print(note.rstrip("\n"))
                print("")
                flag_occurances += new_occurances
            # End if new_notes
        # End for script
        
        if flag_occurances > 0:
            print(colors.RED + "Total " + flag + " occurances: " 
                 +str(flag_occurances) + colors.ENDCOLOR)
        else:
            print(colors.GREEN + "Total " + flag + " occurances: "
                 +"none" + colors.ENDCOLOR)
        print("-------------------------\n")
    # End for flag
    
    sys.exit(0)
# End main
    
def scan_lines(lines, flag):
    notes = []
    occurances = 0
    # Break lines array with a for loop. Uses indexing by i instead of
    # breaking directly because the index is important info.
    for i in range(0, len(lines)):
        line = lines[i]
        # Looks for flags in text in format such as "# DEV - "
        if ("# " + flag + " - ") in line:
            occurances += 1
            # code and sep are unwanted return arguments of "partition"
            code, sep, note = line.partition("# " + flag + " - ")
            notes.append(flag + " - " + note)

            # Check the following lines for two space indent, signifying
            # continued note. lines[j] will always be the next line.
            j = i + 1
            while "#  " in lines[j]:
                code, sep, note = lines[j].partition("#   ")
                notes.append("  " + note)
                j += 1
            # End while format in next line
        # End if format in line
    # End for i

    return notes, occurances
# End scan_lines


main()

