extends Node

# DEV - Improve the queue handler so that the debug key function, as
#   defined in game_screen.gd to immediately reset the battle, will
#   work safely.
# 
# queue_handler is designed to be instanced , usually only once for the
# scope of the entire program, in the game_screen class to manage the
# timed triggering of one or more "events", where events can be
# functions within the script using this queue_handler.
# 
# To work properly, it is expected that you add queue_handler to the
# tree, preferably under game_screen, so that _ready() is triggered. In
# addition, game_screen is expected to have the following functions:
# pause_for(time, initiator, callback)
# describe(message)
# 
# If there is a function that needs to be called as soon as possible
# but only after a timed event finishes, you can queue the function to
# be called as an event, assuming the previous timed event was also in
# the queue. For this reason, most things that happen should go into
# the queue_handler.
#
# Variables:
# event_finished
#   This signal is connected to game_screen just as the event is
#   created, tying it to the desired function to call at the close.
# 
# game_screen
#   The expected creator of this class's instance
# 
# _event_queue
#   A 2D array where each array element has 3 elements:
#	time, callback, and message

var game_screen
var _event_queue = []

func _ready():
	game_screen = Global.get_game_screen()
	

# Adds an event to the queue. An event is simply a function call,
# followed by a timed pause, then a call to _finish_event, where a new
# event may be fired if there are more in the event_queue. The event's
# callback function is triggered in start_next_event.
# 
# The args are grouped into an array, representing one event, that is
# stored in event_queue, an array of "events". The order of the
# arguments in the "event" differs from the order passed in. The
# justification for storing out of the order passed in is to make more
# sense when it is parsed out in _finish_event.
# 
# The events in the queue go from newest on the front or "right" of the
# array to oldest on the back or "left" of the array.
# 
# Args
# time - duration of event after calling callback before "finishing" 
# callback - function to execute, possibly delayed by the event_queue
# message - description drawn to screen. OPTIONAL.
func queue_event(callback, message = null, time = Global.game_speed):
	Global.update_log("queuing event: " + callback, Global.DEBUG)
	_event_queue.append([time, callback, message])
	
	if len(_event_queue) == 1: # i.e. the only event is the one just added
		_start_next_event()
	

# This function has an extreme name to signify its dangerous power
func destroy_queue():
	_event_queue = []
	

# Deletes the event that just completed its associated wait time, then
# starts the next event if there is one
func _finish_event():
	_event_queue.pop_front()
	if len(_event_queue) > 0:
		_start_next_event()
	

# Runs the next event. Events are followed by timed pause, after which
# point, _finish_event is called to increment the queue and start the
# next event.
func _start_next_event():
	var next_event = _event_queue[0]
	var time = next_event[0]
	var callback = next_event[1]
	var message = next_event[2]
	
	game_screen.call(callback)
	
	if message != null:
		game_screen.describe(message)
	game_screen.pause_for(time, self, "_finish_event")
	
	Global.update_log("started event: " + str(callback), Global.DEBUG)
	var event_queue_names = [] # DEV - For debugging
	for event in _event_queue:
		event_queue_names.append(event[1])
	Global.update_log("current event queue: " + str(event_queue_names), Global.DEBUG)
	