extends Node

# DEV - Rename game_speed to game_delay
# 
# Global can be accessed from anywhere as "Global", since it is in the
# AutoLoad. It holds system-level variables, provides universal
# functions missing from GDScript such as randint, provides functions
# for system tasks such switch_scene, and forwards input events from
# _input to the current scene's forwarded_input function.
# 
# Milestones
# ---------------------------------------------------------------------
# 0.0.3 Alpha:
# Able to quit to menu
# Able to restart at death
# 
# 0.0.4 Alpha:
# Level up
# Multiple classes
# More enemies
# 
# 0.0.5 Alpha:
# Enemy weaknesses
# A design around the random encounters
# Fleshed out class abilites
# 
# 0.1 Beta:
# Menu system
# Sound
# Some way to beat the game
# 
# 0.2 Beta:
# Android port
# 
# 0.3 Beta:
# Multiplayer support
# 
# 0.9 Beta:
# Some sort of "campaign"
# All classes finished
# 
# 1.0:
# Fix bugs from beta
# Good documentation, including how to mod
# ---------------------------------------------------------------------
# 
# Variables:
# _current_scene
#   The parent node of all others, updated by calling "switch_scene"
# 
# version - A string, mainly used for display purposes
# 
# resolution
#   This is the number of "pixels" drawn in the game. Not necessarily
#   screen pixels, but cubes of solid color in the case of upscaling
#   the game window. This is not a guaranteed accurate value, but a
#   recording of what should be the resolution.
# 
# palette
#   Not an enforced color scheme (yet), but rather a means of accessing
#   the correct color scheme when adding color by code
# 
# game_speed
#   The time in seconds per thing happening, usually the game screen
#   describing an action. Higher is slower.
# 
# syslog
#   Holds every message sent to "update_log" along with the
#   corresponding priority in an array of 2 element arrays.

signal reset_pressed
signal debug_pressed
signal pause_pressed

# Game configuration
var mouse_enabled = true

# Variables used mainly by global.gd
var _current_scene
var syslog = []

# Variables used by global.gd and other classes
var version = "0.0.2 Alpha"
var resolution = Vector2(ProjectSettings.get_setting("display/window/size/width"),
						 ProjectSettings.get_setting("display/window/size/height"))
var palette = [Color("a6adae"), Color("76858a"), Color("54586a"), Color("282635")]
var game_speed = 1
#var game_speed = 0.01

func _ready():
	# DEV - This is here for debug purposes. The Godot Editor's "Output",
	#   useful in debugging, may not scroll to the bottom until you do so
	#   manually, so this gives an opportunity for the developer to do so
	#   early on.
	print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
	randomize() # Generates seed based on time
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	# Set up the splash for its short display
	# DEV - Use Godot splash instead
	_current_scene = get_node("/root/Splash")
	_current_scene.pause_for(game_speed * 2, self, "end_splash")
	

func randint(low, high):
	var num = randi() % (high - low + 1) + low
	
	# BUG - the internal number generator shouldn't need a new seed after
	#   every number generated, but not including this line breaks
	#   something.
	# This is apparently needed if randi() is to be called more than once
	randomize()
	return num
	

func logbase(base, num):
	# Utilize the change of base formula
	return (log(num) / log(base))
	

# DEV - Use boolean math to parse clumped enums for efficiency
# 
# Returns an array of enums based off of one enum presumed to be
# "clumped". A "clumped_enum", like a regular enum, actually holds a
# numerical value. If a series of enums are assigned explicit values
# where each is double the last, then the sum of each combination of 2
# or more enums is unique, and thus one numerical value can represent
# several enums. That numerical value would be considered a "clumped
# enum".
# 
# As an example, if enum {A = 1, B = 2, C = 4}, then:
# A + B = 3, A + C = 5, B + C = 6, and A + B + C = 7.
# 
# In the above example each number 1 through 7 represents a unique
# combination of between 1 and 3 enums. If you call "declump_enum" with
# an enum = 6, this function will return the array [2, 4].
# 
# For simplicity's sake, it is assumed that clumped_enum is composed of
# enums starting with the value 1, so 1, 2, 4, 8...
func declump_enum(clumped_enum):
	# The highest enum will be 2^x power. the log (base 2) of the
	# clumped_enum will return some number >= x, and the floor of the
	# returned number will == 2^x. Continue until clumped_enum becomes 1.
	var declumped_enums = []
	while clumped_enum > 0:
		var highest_enum = pow(2, floor(logbase(2, clumped_enum)))
		declumped_enums.append(highest_enum)
		clumped_enum = clumped_enum - highest_enum
	return declumped_enums
	

func _input(event):
	if event.is_action_pressed("fullscreen"):
		toggle_fullscreen()
	elif event.is_action_pressed("reset"):
		emit_signal("reset_pressed")
	elif event.is_action_pressed("debug"):
		emit_signal("debug_pressed")
	elif event.is_action_pressed("menu_1"):
		emit_signal("pause_pressed")
	
	_current_scene.forwarded_input(event)
	

# The numbers for each priority correspond to syslog severity levels,
# with some excluded.
# Comment the blocks for the types of messages you want ignored.
# FEAT - Color code syslog message priorities
enum {ALERT = 1, ERROR = 3, WARNING = 4, INFO = 6, DEBUG = 7}
func update_log(message, priority = INFO):
	# These are the highest-level errors. Most likely to crash the game or
	# do permanent damage to a save.
	if priority == ALERT:
		print(message)
		# ALERT priority problems should be debugged immediately, but you can
		# comment "breakpoint" to disable it if you care as much as Phil
		# Collins (he doesn't care anymore).
		breakpoint
	
	# These report bugs that can affect gameplay.
	if priority == ERROR:
		print(message)
	
	# These report unexpected behavior that won't directly affect
	# gameplay, possibly because the program handled the unexpected
	# behavior. Can also include reminders to devs, but generally
	# don't use this priority level.
	if priority == WARNING:
		print(message)
	
	# Normal status updates that someone reading the console would hope
	# to receive if checking the print output. It should be concise.
	if priority == INFO:
		print(message)
	
	# Debug status updates can be more detailed than info status updates.
	# They are more detailed for debugging a specific problem, but then
	# the lines of code that output debug status should probably be
	# removed with the removal of the bug. If more bugs may crop up that
	# would need similar debug messages reimplemented, you can leave the
	# lines of code that output debug status, but also consider
	# categorizing such a status update as info.
	if priority == DEBUG:
		print(message)
	
	syslog.append([message, priority])
	

func end_splash():
	switch_scene("MainMenu")
	

func start_game():
	switch_scene("GameScreen")
	

func show_level_up():
	switch_scene("LevelUpMenu")
	

func show_game_over():
	switch_scene("GameOver")
	

# switch_scene
# Args- scene name: This is the filename of the scene to instance
#         without the extension.
# 
# This function changes the scene tree to contain the new scene at the
# nodepath /root/SceneName. The old scene that was just below root will
# be freed. The variable _current_scene is updated to the new scene.
# 
func switch_scene(scene_name):
	var new_top_node = load("res://scenes/ui/" + scene_name + ".tscn").instance()
	var old_scene = _current_scene
	_current_scene = new_top_node
	
	get_tree().get_root().add_child(new_top_node)
	get_tree().set_current_scene(new_top_node)
	
	old_scene.queue_free()
	

func toggle_fullscreen():
	if OS.is_window_fullscreen():
		OS.set_window_fullscreen(false)
	else:
		OS.set_window_fullscreen(true)
	

func shutdown():
	get_tree().quit()
	

func restart():
	switch_scene("MainMenu")
	

func get_current_scene():
	return _current_scene
	

func get_splash():
	if _current_scene.name == "Splash":
		return _current_scene
	else:
		return null
	

func get_main_menu():
	if _current_scene.name == "MainMenu":
		return _current_scene
	else:
		return null
	

func get_game_screen():
	if _current_scene.name == "GameScreen":
		return _current_scene
	else:
		return null
	