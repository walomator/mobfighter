extends "res://scripts/ui/list_menu.gd"

var game_screen
var player

func _ready():
	game_screen = Global.get_game_screen()
	player = game_screen.player
	

func handle_item_selected(selection):
	if   selection == 0:
		# FEAT - Doesn't account for multiple enemies)
		player.cast_spell("Flames", [game_screen.mobs[0]])
		close_menu()
	
	elif selection == 1:
		player.cast_spell("Ice", [game_screen.mobs[0]])
		close_menu()
	