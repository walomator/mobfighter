extends "res://scripts/game_classes/game_class.gd"

var skills_menu_scene = preload("res://scenes/ui/MageSkillsMenu.tscn")


func _init(creator):
	player = creator
	class_name = "Mage"
	
	player.set_level(1)
	player.set_max_hp(3 + Global.randint(4, 6))
	player.set_max_mp(3)
	player.max_out()
	# For a balanced class, these stats should add up to 16
	player.set_strn(1)
	player.set_con(3)
	player.set_dex(1)
	player.set_foc(5)
	player.set_wis(3)
	player.set_intl(4)
	player.spellbook.learn_spells(["Flames", "Ice"])
	

func attack(mob):
	default_attack(mob)
	

func open_skills_menu():
	var sub_ui = player.ui.create_sub_ui()
	sub_ui.create_menu(sub_ui.ACTIVE, skills_menu_scene)
	

#const FLAMES_COST = 3
#func cast_flames(mob):
#	var success = player.initiate_spell("Flames", FLAMES_COST)
#	if success:
#		var damage = Global.randint(3, 10) + floor(player.get_foc() / 2)
#		mob.decrease_hp(damage)
#
#	player.end_turn()
#

#const ICE_COST = 3
#func cast_ice(mob):
#	var success = player.initiate_spell("Ice", ICE_COST)
#	if success:
#		var damage = Global.randint(6, 7) + floor(player.get_foc() / 2)
#		mob.decrease_hp(damage)
#
#	player.end_turn()
#