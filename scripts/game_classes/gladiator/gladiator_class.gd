extends "res://scripts/game_classes/game_class.gd"

var skills_menu_scene = preload("res://scenes/ui/GladiatorSkillsMenu.tscn")
var javelin_count = 3

func _init(creator):
	player = creator
	class_name = "Gladiator"
	
	player.set_level(1)
	player.set_max_hp(6 + Global.randint(4, 6))
	player.set_max_mp(0)
	player.max_out()
	# For a balanced class, these stats should add up to 16
	player.set_strn(5)
	player.set_con(3)
	player.set_dex(4)
	player.set_foc(1)
	player.set_wis(2)
	player.set_intl(1)
	

func attack(mob):
	default_attack(mob)
	

func throw_javelin(mob):
	if javelin_count <= 0:
		player.ui.describe("You are out of javelins!")
	else:
		javelin_count -= 1
		player.ui.describe("You throw a javelin")
		
		var damage = Global.randint(5, 7) + floor(player.get_strn() / 2)
		mob.decrease_hp(damage)
	
	player.end_turn()
	

func shield():
	player.add_effect("resist_damage")
	player.ui.describe("You raise your shield")
	player.end_turn()
	

func open_skills_menu():
	var sub_ui = player.ui.create_sub_ui()
	sub_ui.create_menu(sub_ui.ACTIVE, skills_menu_scene)
	