extends "res://scripts/ui/list_menu.gd"

var game_screen
var player
var saved_selection

func _ready():
	game_screen = Global.get_game_screen()
	player = game_screen.player
	game_screen.connect("mob_selected", self, "handle_mob_selected")
	

func handle_item_selected(selection):
	if   selection == 0:
#		player.game_class.throw_javelin(game_screen.mobs[0])
		saved_selection = selection
		
		self.get_parent().visible = false
		game_screen.switch_active_ui(game_screen)
		game_screen.open_mob_select_menu()
	
	elif selection == 1:
		game_screen.describe("You throw a net")
		player.end_turn()
		close_menu()
	
	elif selection == 2:
		game_screen.describe("Your vision becomes clouded")
		player.end_turn()
		close_menu()
	

# BUG - Throwing the javelin can lead to a crash when "mobs" is empty
#   with:
#   "Invalid get index '0' (on base: 'Array')."
#   Why the mobs array is empty when a mob was just selected is a very
#   strange, unsolved mystery.
#   See also: bug in game_screen.gd caused by throwing the javelin.
func handle_mob_selected(mob_selection):
	# The selection was the item you initially selected from this menu
	if saved_selection == 0:
		player.game_class.throw_javelin(game_screen.mobs[mob_selection])
	
	elif saved_selection == 1:
		pass
	
	elif saved_selection == 2:
		pass
	
	close_menu()
	