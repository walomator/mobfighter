extends Node

# This is the generic class. The preceding sentence is a pun, because
# "classes" in RPGs describe different types of character builds. To
# help distinguish which "class" is being talked about, "game class"
# will hereafter be used to mean the RPG sense of the word.
# 
# In MobFighter, the player entity creates a game_class object. This
# object will do most actions that require lookup of stats or do
# behavior fitting of a game class, but to do so requires a lookup of
# the player's stats and calling a player function to set variables.
# 
# In this sense a game_class can be said to be a player "controller";
# this class does not increase its own stats, nor does it have any, but
# it controls the stats of the player.
# 
# Different game classes, e.g., the warlock, inherit from this generic
# class (script).
# 
# FEAT - menu item "spells" becomes "skills" when there is no magic

var player
var class_name = "missing_class_name"

var favored_attributes

# This _init function must be included in every subclass of
# game_class.gd, as _init functions are not inherited.
#func _init(creator):
#	player = creator
	

# In most cases, an attack will function identically between classes,
# so use this function to avoid repeating code.
func default_attack(mob):
	player.ui.describe("You attack")
	
	var damage = Global.randint(1, 2) + floor(player.get_strn() / 2)
	mob.decrease_hp(damage)
	
	player.end_turn()
	

func open_skills_menu():
	pass
	
