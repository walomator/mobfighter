# MobFighter

## About
Also called MOBFGHTR by some nostalgic types.

There are no platform-specific builds at this time. The game engine used is
Godot 3.0.5 (not included). You can run this game by using the Godot editor and
scanning this folder, then playing in the editor or exporting.

I hope to flesh this game out, however the target visual style will remain
retro.

## Legal Notice
This program is free software (GPLv3). See the accompanying LICENSE if you'd
like to know your rights.

All non-code content is released under the Creative Commons CC0 license,
effectively making it public domain, unless otherwise specified.

Exceptions include:
* BitPotion font by Joel Rogers, licensed CC-BY
(https://joebrogers.itch.io/bitpotion). Consider donating!

Beyond the exceptions, others' work will be credited when possible, even if the
license does not require credit.

Other used works:
* Tom Thumb font by Brian Swetland and robey, licensed CC0
(https://robey.lag.net/2010/01/23/tiny-monospace-font.html).

See the accompanying LICENSE-CC-BY and LICENSE-CC0 for the licenses' legal
code and/or read an easy to understand summary on the Creative Commons website.
* [CC-BY](https://creativecommons.org/licenses/by/4.0)
* [CC0](https://creativecommons.org/publicdomain/zero/1.0/)
